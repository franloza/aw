<!DOCTYPE html>
<html>
	<?php include ('admin-head.html'); ?>
  	<body>
 	<div class="container">
	<?php include ('admin-menu.html'); ?>
	<?php include ('admin-titlebar.html'); ?>
	<div id="content-perfil" class="content">    
		<div id="imagen-estudiante">
			<IMG SRC="img/kakshi.jpg">
		</div>
		<div class="contenedor-perfil">
			<h2><strong>Luna Maria Moreno Sanchez</strong></h2>
			<p>Probador de paginas web y su utilidad, disenadora de bases de datos</p>
			<p>Madrid zona centro y norte</p>
			<table class="admin-table">
				<tr>
					<td>Actual</td>
					<td>Programadora de bases de datos para banco Caminos</td>
				</tr>
				
				<tr>
					<td>Anterior</td>
					<td>Becaria en la Universidad Complutense. Tecnica de Infomatica</td>
				</tr>
				
				<tr>
					<td>Educacion</td>
					<td>UCM-Informatica</td>
				</tr>
				
				<tr>
					<td>Idiomas</td>
					<td>Ingles, Frances y aleman</td>
				</tr>
			</table>
		</div>

		<div id="admin-aptitudes" class="aptitudes"> 
			<ul>
				<li class="admin-table"><strong>Aptitudes</strong></li>
				<li class="li2">Trabajador</li>
				<li class="li2">Optimista</li>
				<li class="li2">Lider</li>
				<li class="li2">Extrovertido</li>
				<li class="li2">Alegre</li>
				<li class="li2">Inconformista</li>
				<li class="li2">Ambicioso</li>
				<li class="li2">Leal</li>
			</ul>

		</div>
		<div id="informacion-personal">
			<p><strong>Informacion Personal</strong></p>
			<table class="admin-table">
				<tr>
					<td>eMail</td>
					<td>LunaMGS@ucm.es</td>
				</tr>

				<tr>
					<td>telefono movil</td>
					<td>608 85 66 47</td>
				</tr>

				<tr>
					<td>telefono fijo</td>
					<td> 91 857 24 89</td>
				</tr>

			</table>


		</div>

		<div id="documentacion-adicional">
			<a href="cvLuna.docx"><strong>Curriculum Vitae</strong></a>

		</div>

	</div>
	<?php include ('admin-footer.html'); ?>
  </div>
  </body>
</html>
