<!DOCTYPE html>
<html>
<?php include ('empresa-head.html'); ?>
  <body>
  <div id="container-ofertas-ver" class="container">
	<?php include ('empresa-menu.html'); ?>
	<?php include ('empresa-titlebar.html'); ?>
	<div id="content-ofertas-ver2" class="content">
        <div id="tabla-ofertas-ver2" class="table-container">
            <div class="table-header"> Oferta de prácticas </div>
             <table class="empresa-table">
                <tr>
                    <td>Empresa</td>
                    <td>Rediris</td>
                </tr>
                <tr>
                    <td>Puesto</td>
                    <td>Tecnico de seguridad</td>
        		</tr>
                <tr>
                    <td>Sueldo</td>
                    <td>400€</td>
                </tr>
                <tr>
                    <td>Horas</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>Plazas</td>
                    <td>2</td>
                </tr>
        		<tr>
                    <td>Duracion</td>
                    <td>4 meses</td>
                </tr>
                <tr>
                    <td>Disponibilidad</td>
                    <td>mañana</td>
        		</tr>
                <tr>
                    <td>Funciones</td>
                    <td>Creacion y mantenimiento de scripts que evaluen la seguridad de las ip's consultadas a traves de los routers establecidos</td>
                </tr>
                <tr>
                    <td>Aptitudes</td>
                    <td><p>Perl</p>
                        <p>Python</p>
                        <p>Estudiante</p>
                        <p>Disponibilidad</p>
                        <p>Seguridad</p></td>
                </tr>
                <tr>
                    <td>Requisitos minimos</td>
                    <td><p>Estar en ultimo año de carrera</p>
                        <p>Nivel B1 de ingles</p>
                        <p>Experiencia con uso de iptables y firewall</p></td>

                </tr>
                <tr>
                    <td>Aconsejable</td>
                    <td><p>Facilidad para trabajar en grupo</p>
        				<p>Conocimientos basicos de protocolos de red</p></td>
                </tr>
                </table>
            </div>
        </div>
	</div>
    <?php include ('empresa-footer.html'); ?>
  </div>
  </body>
</html>
