<!DOCTYPE html>
<html>
<?php include ('empresa-head.html'); ?>
  <body>
  <div id="container-contratos-ver" class="container">
	<?php include ('empresa-menu.html'); ?>
	<?php include ('empresa-titlebar.html'); ?>
	<div id="content-contratos-ver" class="content">
        <div id="tabla-contratos-ver" class="table-container">
            <div class="table-header"> Contrato de prácticas </div>
                <table class="empresa-table">
                <tr>
                    <td>Empresa</td>
                    <td>Intel</td>
                </tr>
                <tr>
                    <td>Estudiante</td>
                    <td>Antonio Perez Sanchez</td>
                </tr>
                <tr>
                    <td>Estudios</td>
                    <td>Ingeniería informática</td>
                </tr>
                <tr>
                    <td>Puesto</td>
                    <td>Programador Java</td>
        		</tr>
                <tr>
                    <td>Sueldo</td>
                    <td>400€</td>
                </tr>
                <tr>
                    <td>Horas</td>
                    <td>20</td>
                </tr>

        		<tr>
                    <td>Fecha de inicio del contrato</td>
                    <td>12/03/2014</td>
                </tr>
                <tr>
                    <td>Fecha de finalización del contrato</td>
                    <td>30/07/2014</td>
        		</tr>
                <tr>
                    <td>Funciones</td>
                    <td>Mantenimiento de la red y de las conexiones de la empresa, como a su vez dar soporte de ayuda a las diferentes
        				estructuras dentro de la red empresarial con sus colaboradores</td>
                </tr>
                </table>
            </div>
        </div>
	</div>
    <?php include ('empresa-footer.html'); ?>
  </div>
  </body>
</html>