<!DOCTYPE html>
<html>
	<?php include ('empresa-head.html'); ?>
  	<body>
  	<div id="container-empresa-perfil" class="container">
		<?php include ('estudiante-menu.html'); ?>
		<?php include ('estudiante-titlebar.html'); ?>
		
		<div class="content">
		<div class="perfil-empresa-content">
			<div class="titulo-perfil">
			<div class="logo-perfil">
			<img src="img/rediris.jpg" width="200" height="180"></img>
			</div>
			<div class="datos-logo-perfil">
				<div class="text-datos-logo">
					<h3 id="nombre-empresa">RedIRIS</h3>
					<p class="text-muted">Investigacion</p>
					<p class="text-muted">De 11 a 50 empleados</p>
				</div>
			</div>
		</div>
		<div class="descripcion-perfil">
			<p>RedIRIS es la red académica y de investigación española y proporciona servicios avanzados de comunicaciones a la comunidad científica y universitaria nacional. Está financiada por el Ministerio de Economía y Competitividad, e incluida en su mapa de Instalaciones Científico-Técnicas Singulares (ICTS). Se hace cargo de su gestión la entidad pública empresarial Red.es, del Ministerio de Industria, Energía y Turismo.
			</p>
			<p>RedIRIS cuenta con más de 500 instituciones afiliadas, principalmente universidades y centros públicos de investigación, que llegan a formar parte de esta comunidad mediante la firma de un acuerdo de afiliación.
			</p>
			<p>Se puede consultar información de detalle sobre RedIRIS y sus principales líneas de actuación en: 
			</p>
		</div>
		<div class="datos-interes-perfil">
			<div class="bloque">
				<strong>Sitio web</strong>
				<p class="text-muted">http://www.rediris.es/</p>
				<strong>Sede</strong>
				<p class="text-muted">
					Madrid Edificio Bronce Plaza
					Manuel Gomez Moreno, s/n - 2ª
					planta Madrid, Madrid 28020
					España
				</p>
			</div>		
			<div class="bloque">
				<strong>Sector</strong>
				<p class="text-muted">Investigacion</p>
				<strong>Tamaño de empresa</strong>
				<p class="text-muted">De 11 a 50 empleados</p>
			</div>		
			<div class="bloque">
				<strong>Tipo</strong>
				<p class="text-muted">Empresa publica</p>
				<strong>Fundacion</strong>
				<p class="text-muted">1998</p>
			</div>		
			<div class="clear"></div>
		</div>
		</div>
		</div>
  		
  		<?php include ('estudiante-footer.html'); ?>
  	</div>
  </body>
</body>
</html>
