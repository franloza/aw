<!DOCTYPE html>
<html>
<?php include ('admin-head.html'); ?>
  <body>
  <div id="container-ofertas-ver" class="container">
	<?php include ('admin-menu.html'); ?>
	<?php include ('admin-titlebar.html'); ?>
	<div id="content-ofertas-ver" class="content">
        <div id="tabla-ofertas-ver" class="table-container">
             <div class="table-header"> Encuesta Estudiante - Resuelta </div>
                <table class="admin-table">
                <tr></tr>
                <tr>
                    <td>¿Te gustan las gallinas?</td>
                    <td>Mmmm... Si</td>
                </tr>
                <tr>
                    <td>¿Por qué crees que deberiamos contratarte a ti en vez de a otro?</td>
                    <td>Debido a mis ganas de superación dia a dia</td>
                </tr>
                <tr>
                    <td>¿Cuáles son tus principales cualidades?</td>
                    <td>Soy un gran programador y me gustan las gallinas..</td>
                </tr>
                <tr>
                    <td>¿En la pregunta anterior respondiste que tu mayor cualidad es que gustan las gallinas?</td>
                    <td>Si</td>
        		</tr>
                <tr>
                    <td>¿Qué tipo de lenguajes de programación dominas?</td>
                    <td>Java, c++</td>
                </tr>
                <tr>
                    <td>¿Por qué despues de leer la pregunta penultima a esta has cambiado la respuesta a la antepenultima?</td>
                    <td>No lo he hecho...</td>
                </tr>

        		<tr>
                <td>¿Cuánto te gustaria cobrar en nuestra empresa?</td>
                <td>Lo que ustedes consideren oportuno para un becario</td>

                </tr>
                <tr>
                  <td>¿Te gustan los gatos?</td>
                  <td>No...</td>
        		</tr>
                <tr>
                    <td>¿Cuánto tiempo te gustaría estar en nuestra empresa?</td>
                    <td>Todo lo que se me permita</td>
                </tr>
                <tr>
                    <td>¿Sabes que los gatos dañan a las gallinas?</td>
                    <td>...</td>
                </tr>
                </table>
            </div>
        </div>
	</div>
    <?php include ('admin-footer.html'); ?>
  </div>
  </body>
</html>
